@REM @Author: Bartuccio Antoine
@REM @Date:   2019-06-11 17:31:39
@REM @Last Modified by:   klmp200
@REM Modified time: 2019-06-16 20:40:45

@echo off
set DIR=%~dp0%

echo Downloading package manager
powershell -NoProfile -ExecutionPolicy Bypass -Command "((new-object net.webclient).DownloadFile('https://chocolatey.org/install.ps1','%DIR%install.ps1'))"
echo Runing package manager installer
powershell -NoProfile -ExecutionPolicy Bypass -Command "& '%DIR%install.ps1' %*"

echo Installing make
choco install make -y
echo Installing cmake
choco install cmake --installargs 'ADD_CMAKE_TO_PATH=System' -y
echo Installing git
choco install git.commandline -y
echo Installing mingw
choco install mingw -y

echo Installation complete, press any key to continue
pause
