#include "camera.hpp"
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "shader_config.hpp"

Camera::Camera(glm::vec3 &eye, glm::vec3 &direction, glm::vec3 &up, int width, int height)
{
    this->eye = eye;
    this->direction = direction;
    this->up = up;
    this->width = width;
    this->height = height;

    this->updateUpKey = false;
}

Camera::~Camera() {}

const glm::vec3 &Camera::getEye() const { return eye; }
const glm::vec3 &Camera::getDirection() const { return direction; }
const glm::vec3 &Camera::getUp() const { return up; }
const ShaderProgram &Camera::getShaderProgram() const { return shaderProgram; }

void Camera::setShaderProgram(ShaderProgram &program)
{
    this->shaderProgram = program;
}

void Camera::setUp(const glm::vec3 up)
{
    this->up = up;
}

void Camera::translate(const glm::vec3 translation)
{
    this->eye += translation;
}

void Camera::rotate(const glm::vec3 axis, const float angle)
{
    glm::quat rot(
        glm::cos(angle / 2),
        axis.x * glm::sin(angle / 2),
        axis.y * glm::sin(angle / 2),
        axis.z * glm::sin(angle / 2));
    rot = glm::normalize(rot);
    this->direction = glm::normalize(rot * this->direction);
    this->up = glm::normalize(rot * this->up);
}

void Camera::rotateAround(const glm::vec3 target, const glm::vec3 axis, const float angle)
{
    this->eye = this->eye - target;
    glm::quat rot(
        glm::cos(angle / 2),
        axis.x * glm::sin(angle / 2),
        axis.y * glm::sin(angle / 2),
        axis.z * glm::sin(angle / 2));
    rot = glm::normalize(rot);
    this->eye = rot * this->eye;
    this->eye = this->eye + target;
}

void Camera::lookAt(const glm::vec3 target)
{
    this->direction = glm::normalize(target - this->eye);
}

glm::mat4 Camera::getViewMatrix() const
{
    return glm::lookAt(
        this->eye,
        this->eye + this->direction,
        this->up);
}

void Camera::handleInputs(GLFWwindow *window, float delta)
{
    if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS && (!updateUpKey))
    {
        if (up == glm::vec3(0, 1, 0))
            up = glm::vec3(0, -1, 0);
        else if (up == glm::vec3(0, -1, 0))
            up = glm::vec3(0, 0, 1);
        else if (up == glm::vec3(0, 0, 1))
            up = glm::vec3(0, 0, -1);
        else if (up == glm::vec3(0, 0, -1))
            up = glm::vec3(1, 0, 0);
        else if (up == glm::vec3(1, 0, 0))
            up = glm::vec3(-1, 0, 0);
        else
            up = glm::vec3(0, 1, 0);
        setUp(up);
        updateUpKey = true;
    }
    if (glfwGetKey(window, GLFW_KEY_F1) != GLFW_PRESS)
    {
        updateUpKey = false;
    }
}

void Camera::glUse(ShaderProgram shader)
{
    GLuint loc = glGetUniformLocation(shader.getProgramId(), SHADER_CAMERA_POSITION_NAME);
    glUniform3f(loc, eye.x, eye.y, eye.z);
}

CameraFree::CameraFree(glm::vec3 &eye, glm::vec3 &direction, glm::vec3 &up, int width, int height) : Camera(eye, direction, up, width, height) {}

void CameraFree::handleInputs(GLFWwindow *window, float delta)
{
    //mouse handling
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    int radius = 100;
    float angularSpeed = 0.05;
    float speed = 0.05;

    double xoffset = xpos - this->width / 2;
    double yoffset = ypos - this->height / 2;

    if (glm::abs(xoffset) >= radius)
    {
        this->rotate(this->getUp(), -xoffset * angularSpeed * delta);
    }
    if (glm::abs(yoffset) >= radius)
    {
        this->rotate(glm::cross(this->direction, this->up), -yoffset * angularSpeed * delta);
    }

    //keyboard handling
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        this->translate(speed * delta * glm::vec3(this->direction));
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        this->translate(speed * delta * glm::vec3(-this->direction));
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        this->translate(-speed * delta * glm::cross(this->direction, this->up));
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        this->translate(speed * delta * glm::cross(this->direction, this->up));
    }

    Camera::handleInputs(window, delta);
}

CameraRotateAround::CameraRotateAround(glm::vec3 &eye, glm::vec3 &center, glm::vec3 &up, int width, int height) : center(center), Camera(eye, eye, up, width, height)
{
    this->direction = center - eye;
}

const glm::vec3 &CameraRotateAround::getCenter() const
{
    return this->center;
}

void CameraRotateAround::handleInputs(GLFWwindow *window, float delta)
{
    //mouse handling
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    glfwSetCursorPos(window, this->width / 2, this->height / 2);

    int radius = 100;
    float angularSpeed = 0.01;

    double xoffset = xpos - this->width / 2;
    double yoffset = ypos - this->height / 2;

    this->rotateAround(this->center, up, -xoffset * angularSpeed * delta);
    this->rotateAround(this->center, glm::normalize(glm::cross(up, this->center - this->eye)), -yoffset * angularSpeed * delta);
    this->lookAt(this->center);

    Camera::handleInputs(window, delta);
}