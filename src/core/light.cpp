#include "light.hpp"

#include "shader_config.hpp"

Light::Light()
{
    type = AMBIANT;
    position = glm::vec3(0);
    attenuation = glm::vec2(0);
    color = glm::vec3(0);
}
Light Light::setAmbianteLight(glm::vec3 _color)
{
    type = AMBIANT;
    position = glm::vec3(0);
    attenuation = glm::vec3(0);
    direction = glm::vec3(0);
    color = _color;
    return *this;
}

Light Light::setAmbianteLight(float power)
{
    type = AMBIANT;
    position = glm::vec3(0);
    attenuation = glm::vec3(0);
    direction = glm::vec3(0);
    color = glm::vec3(power);
    return *this;
}
Light Light::setPointLight(glm::vec3 _position, glm::vec3 _color,
                           glm::vec2 _attenuation)
{
    type = POINT;
    position = _position;
    attenuation = _attenuation;
    direction = glm::vec3(0);
    color = _color;
    return *this;
}
Light Light::setPointLight(glm::vec3 _position, float power,
                           glm::vec2 _attenuation)
{
    type = POINT;
    position = _position;
    attenuation = _attenuation;
    direction = glm::vec3(0);
    color = glm::vec3(power);
    return *this;
}
Light Light::setDirectionalLight(glm::vec3 _direction, glm::vec3 _color)
{
    type = DIRECTIONAL;
    position = glm::vec3(0);
    attenuation = glm::vec3(0);
    direction = _direction;
    color = _color;
    return *this;
}
Light Light::setDirectionalLight(glm::vec3 _direction, float power)
{
    type = DIRECTIONAL;
    position = glm::vec3(0);
    attenuation = glm::vec3(0);
    direction = _direction;
    color = glm::vec3(power);
    return *this;
}

void Light::glUse(ShaderProgram shader, unsigned int id)
{
    GLuint loc = Light::getLightVarLoc(shader, id, SHADER_LIGHT_TYPE_NAME);
    glUniform1i(loc, type);

    loc = Light::getLightVarLoc(shader, id, SHADER_LIGHT_POSITION_NAME);
    glUniform3f(loc, position.x, position.y, position.z);

    loc = Light::getLightVarLoc(shader, id, SHADER_LIGHT_DIRECTION_NAME);
    glUniform3f(loc, direction.x, direction.y, direction.z);

    loc = Light::getLightVarLoc(shader, id, SHADER_LIGHT_ATTENUATION_NAME);
    glUniform2f(loc, attenuation.x, attenuation.y);

    loc = Light::getLightVarLoc(shader, id, SHADER_LIGHT_COLOR_NAME);
    glUniform3f(loc, color.r, color.g, color.b);
}

void Light::glSetLightCount(ShaderProgram shader, unsigned int count)
{
    if (count > SHADER_LIGHT_ARRAY_MAX_SIZE)
    {
        std::cerr << "\033[1;31;47mError, try to use " << count
                  << " light but max light count is " << SHADER_LIGHT_ARRAY_MAX_SIZE
                  << "\033[0m" << std::endl;
        exit(0);
    }
    GLuint loc =
        glGetUniformLocation(shader.getProgramId(), SHADER_LIGHT_ARRAY_SIZE_NAME);
    glUniform1i(loc, count);
}
void Light::glEnableLight(const ShaderProgram shader, bool enableAmbiantLight,
                          bool enableDiffuseLight, bool enableSpecularLight)
{
    GLuint loc;
    loc = glGetUniformLocation(shader.getProgramId(),
                               SHADER_LIGHT_AMBIANT_ENABLE_NAME);
    glUniform1i(loc, enableAmbiantLight ? 1 : 0);

    loc = glGetUniformLocation(shader.getProgramId(),
                               SHADER_LIGHT_DIFFUSE_ENABLE_NAME);
    glUniform1i(loc, enableDiffuseLight ? 1 : 0);

    loc = glGetUniformLocation(shader.getProgramId(),
                               SHADER_LIGHT_SPECULAR_ENABLE_NAME);
    glUniform1i(loc, enableSpecularLight ? 1 : 0);
}

GLuint Light::getLightVarLoc(const ShaderProgram shader, unsigned int id,
                             std::string field)
{
    std::string base = SHADER_LIGHT_ARRAY_NAME;
    std::string uniformeName = base + std::string("[") + std::to_string(id) +
                               std::string("].") + std::string(field);
    return glGetUniformLocation(shader.getProgramId(), uniformeName.c_str());
}