#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <algorithm>
#include <vector>

#include "shaderProgram.hpp"

class Camera
{
public:
  Camera(
      glm::vec3 &eye,
      glm::vec3 &direction,
      glm::vec3 &up,
      int width,
      int height);
  ~Camera();
  const glm::vec3 &getEye() const;
  const glm::vec3 &getDirection() const;
  const glm::vec3 &getUp() const;
  const ShaderProgram &getShaderProgram() const;

  void setShaderProgram(ShaderProgram &program);

  void draw();

  void setUp(const glm::vec3 up);

  void translate(const glm::vec3 translation);

  void rotate(const glm::vec3 axis, const float angle);

  void rotateAround(const glm::vec3 target, const glm::vec3 axis, const float angle);

  void lookAt(const glm::vec3 target);

  virtual void handleInputs(GLFWwindow *window, float delta);

  glm::mat4 getViewMatrix() const;

  void glUse(ShaderProgram shader);

protected:
  ShaderProgram shaderProgram;
  glm::vec3 eye;       // Position of the camera
  glm::vec3 direction; // Normal camera vector
  glm::vec3 up;        // Normalized up vector, how the camera is oriented
  int width;
  int height;

  bool updateUpKey;
};

class CameraFree : public Camera
{
public:
  CameraFree(glm::vec3 &eye,
             glm::vec3 &direction,
             glm::vec3 &up,
             int width,
             int height);
  virtual void handleInputs(GLFWwindow *window, float delta);

protected:
};

class CameraRotateAround : public Camera
{
public:
  CameraRotateAround(glm::vec3 &eye,
                     glm::vec3 &center,
                     glm::vec3 &up,
                     int width,
                     int height);
  const glm::vec3 &getCenter() const;
  virtual void handleInputs(GLFWwindow *window, float delta);

protected:
  glm::vec3 center;
};

#endif