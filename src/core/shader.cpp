#include "shader.hpp"
#include <vector>

Shader::Shader(const std::string path, const GLenum shaderType) {
  this->shaderId = glCreateShader(shaderType);
  this->shaderType = shaderType;

  std::string *shader_string = files::readFile(path);
  const char *shader_c_str = shader_string->c_str();
  glShaderSource(shaderId, 1, &shader_c_str, 0);
  glCompileShader(shaderId);

  // display error
  GLint isCompiled = 0;
  glGetShaderiv(shaderId, GL_COMPILE_STATUS, &isCompiled);
  if (isCompiled == GL_FALSE) {
    GLint maxLength = 0;
    glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);

    // The maxLength includes the NULL character
    std::vector<GLchar> errorLog(maxLength);
    glGetShaderInfoLog(shaderId, maxLength, &maxLength, &errorLog[0]);

    std::cout << "\033[1;31;47mShader error:" << std::endl
              << (char *)&errorLog[0] << "\033[0m" << std::endl;
    // Provide the infolog in whatever manor you deem best.
    // Exit with failure.
    glDeleteShader(shaderId); // Don't leak the shader.
    exit(0);
  }

  delete shader_string;
}

Shader::~Shader() {}

const GLuint Shader::getShaderId() const { return shaderId; }
const GLenum Shader::getShaderType() const { return shaderType; }
