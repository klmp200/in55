#ifndef SHADER_PROGRAM_H
#define SHADER_PROGRAM_H

#define GLFW_INCLUDE_GL3
#define GLFW_NO_GLU

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <list>
#include <string>

#include "shader.hpp"

class ShaderProgram {
public:
  ShaderProgram();
  ShaderProgram(const std::list<Shader> shaders);

  ~ShaderProgram();

  const GLuint getProgramId() const;

private:
  GLuint programId;
  std::list<Shader> shaders;
};

#endif