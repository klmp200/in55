#ifndef SHADER_H
#define SHADER_H

#define GLFW_INCLUDE_GL3
#define GLFW_NO_GLU

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <list>
#include <string>

#include "../utils/files.hpp"

class Shader {
public:
  Shader(const std::string path, const GLenum shaderType);

  ~Shader();

  const GLuint getShaderId() const;
  const GLenum getShaderType() const;

private:
  GLuint shaderId;
  GLenum shaderType;
};

#endif