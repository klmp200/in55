#include "material.hpp"

// SOIL
#include <SOIL/SOIL.h>

// standard cpp
#include <iostream>

// project
#include "shader_config.hpp"

Material::Material() {}

Material::Material(const aiMaterial *_material, std::string dir)
{
  material = _material;
  path = dir;
}
std::string Material::getName()
{
  aiString name;
  if (AI_SUCCESS == material->Get(AI_MATKEY_NAME, name))
  {
    return std::string(name.data);
  }
  else // error managment
  {
    std::cerr << "\033[1;31;47mError, can't load texture from material: "
              << getName() << "\033[0m" << std::endl;

    exit(0);
  }
  return std::string("");
}

void Material::glBind()
{
  samplerTextureAmbient = glLoadTextureType(path, aiTextureType_AMBIENT, 0);
  if (samplerTextureAmbient.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureAmbient[0]);
  samplerTextureDiffuse = glLoadTextureType(path, aiTextureType_DIFFUSE, 1);
  if (samplerTextureDiffuse.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureDiffuse[0]);
  samplerTextureDisplacement =
      glLoadTextureType(path, aiTextureType_DISPLACEMENT, 2);
  if (samplerTextureDisplacement.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureDisplacement[0]);
  samplerTextureEmissive = glLoadTextureType(path, aiTextureType_EMISSIVE, 3);
  if (samplerTextureEmissive.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureEmissive[0]);
  samplerTextureHeight = glLoadTextureType(path, aiTextureType_HEIGHT, 4);
  if (samplerTextureHeight.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureHeight[0]);
  samplerTextureLightmap = glLoadTextureType(path, aiTextureType_LIGHTMAP, 5);
  if (samplerTextureLightmap.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureLightmap[0]);
  samplerTextureNormals = glLoadTextureType(path, aiTextureType_NORMALS, 6);
  if (samplerTextureNormals.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureNormals[0]);
  samplerTextureOpacity = glLoadTextureType(path, aiTextureType_OPACITY, 7);
  if (samplerTextureOpacity.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureOpacity[0]);
  samplerTextureReflextion =
      glLoadTextureType(path, aiTextureType_REFLECTION, 8);
  if (samplerTextureReflextion.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureReflextion[0]);
  samplerTextureShininess = glLoadTextureType(path, aiTextureType_SHININESS, 9);
  if (samplerTextureShininess.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureShininess[0]);
  samplerTextureSpecular = glLoadTextureType(path, aiTextureType_SPECULAR, 10);
  if (samplerTextureSpecular.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureSpecular[0]);
  samplerTextureUnknown = glLoadTextureType(path, aiTextureType_UNKNOWN, 11);
  if (samplerTextureUnknown.size() > 0)
    glBindTexture(GL_TEXTURE_2D, samplerTextureUnknown[0]);
}
void Material::glActivate(ShaderProgram shaderProgram)
{

  GLint textureAmbientLoc = glGetUniformLocation(shaderProgram.getProgramId(),
                                                 SHADER_TEXTURE_AMBIENT_NAME);
  GLint textureDiffuseLoc = glGetUniformLocation(shaderProgram.getProgramId(),
                                                 SHADER_TEXTURE_DIFFUSE_NAME);
  GLint textureDisplacementLoc = glGetUniformLocation(
      shaderProgram.getProgramId(), SHADER_TEXTURE_DISPLACEMENT_NAME);
  GLint textureEmissiveLoc = glGetUniformLocation(shaderProgram.getProgramId(),
                                                  SHADER_TEXTURE_EMISSIVE_NAME);
  GLint textureHeightLoc = glGetUniformLocation(shaderProgram.getProgramId(),
                                                SHADER_TEXTURE_HEIGHT_NAME);
  GLint textureLightmapLoc = glGetUniformLocation(shaderProgram.getProgramId(),
                                                  SHADER_TEXTURE_LIGHTMAP_NAME);
  GLint textureNormalsLoc = glGetUniformLocation(shaderProgram.getProgramId(),
                                                 SHADER_TEXTURE_NORMALS_NAME);
  GLint textureOpacityLoc = glGetUniformLocation(shaderProgram.getProgramId(),
                                                 SHADER_TEXTURE_OPACITY_NAME);
  GLint textureReflexionLoc = glGetUniformLocation(
      shaderProgram.getProgramId(), SHADER_TEXTURE_REFLEXION_NAME);
  GLint textureShininessLoc = glGetUniformLocation(
      shaderProgram.getProgramId(), SHADER_TEXTURE_SHININESS_NAME);
  GLint textureSpecularLoc = glGetUniformLocation(shaderProgram.getProgramId(),
                                                  SHADER_TEXTURE_SPECULAR_NAME);
  GLint textureUnknownLoc = glGetUniformLocation(shaderProgram.getProgramId(),
                                                 SHADER_TEXTURE_UNKNOWN_NAME);

  glUniform1i(textureAmbientLoc, 0);
  glUniform1i(textureDiffuseLoc, 1);
  glUniform1i(textureDisplacementLoc, 2);
  glUniform1i(textureEmissiveLoc, 3);
  glUniform1i(textureHeightLoc, 4);
  glUniform1i(textureLightmapLoc, 5);
  glUniform1i(textureNormalsLoc, 6);
  glUniform1i(textureOpacityLoc, 7);
  glUniform1i(textureReflexionLoc, 8);
  glUniform1i(textureShininessLoc, 9);
  glUniform1i(textureSpecularLoc, 10);
  glUniform1i(textureUnknownLoc, 11);

  GLuint hasTexture;
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_AMBIENT_NAME);
  glUniform1i(hasTexture, samplerTextureAmbient.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_DIFFUSE_NAME);
  glUniform1i(hasTexture, samplerTextureDiffuse.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_DISPLACEMENT_NAME);
  glUniform1i(hasTexture, samplerTextureDisplacement.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_EMISSIVE_NAME);
  glUniform1i(hasTexture, samplerTextureEmissive.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_HEIGHT_NAME);
  glUniform1i(hasTexture, samplerTextureHeight.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_LIGHTMAP_NAME);
  glUniform1i(hasTexture, samplerTextureLightmap.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_NORMALS_NAME);
  glUniform1i(hasTexture, samplerTextureNormals.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_OPACITY_NAME);
  glUniform1i(hasTexture, samplerTextureOpacity.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_REFLEXION_NAME);
  glUniform1i(hasTexture, samplerTextureReflextion.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_SHININESS_NAME);
  glUniform1i(hasTexture, samplerTextureShininess.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_SPECULAR_NAME);
  glUniform1i(hasTexture, samplerTextureSpecular.size() > 0);
  hasTexture = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_HAS_TEXTURE_UNKNOWN_NAME);
  glUniform1i(hasTexture, samplerTextureUnknown.size() > 0);

  glSetColorProperty(shaderProgram, SHADER_COLOR_AMBIANT_NAME,
                     AI_MATKEY_COLOR_AMBIENT);
  glSetColorProperty(shaderProgram, SHADER_COLOR_DIFFUSE_NAME,
                     AI_MATKEY_COLOR_DIFFUSE);
  glSetColorProperty(shaderProgram, SHADER_COLOR_EMISSIVE_NAME,
                     AI_MATKEY_COLOR_EMISSIVE);
  glSetColorProperty(shaderProgram, SHADER_COLOR_REFLECTIVE_NAME,
                     AI_MATKEY_COLOR_REFLECTIVE);
  glSetColorProperty(shaderProgram, SHADER_COLOR_SPECULAR_NAME,
                     AI_MATKEY_COLOR_SPECULAR);
  glSetFloatProperty(shaderProgram, SHADER_SHININESS_NAME, AI_MATKEY_SHININESS);
  glSetFloatProperty(shaderProgram, SHADER_SHININESS_STRENGTH_NAME,
                     AI_MATKEY_SHININESS_STRENGTH);
  glSetColorProperty(shaderProgram, SHADER_TRANSPARENT_NAME,
                     AI_MATKEY_COLOR_TRANSPARENT);
}

const aiMaterial *Material::getMaterial() { return material; }

Material::~Material()
{
  // TODO
}

std::vector<GLuint> Material::glLoadTextureType(std::string dir,
                                                enum aiTextureType type,
                                                unsigned int texture)
{
  std::vector<GLuint> vbos;

  unsigned int aux = material->GetTextureCount(type); // texture count
  for (unsigned int i = 0; i < aux; i++)
  {
    aiString path;
    aiTextureMapping mapping;
    unsigned int uvindex;
    float blend;
    aiTextureOp op;
    aiTextureMapMode mapmode;

    // get texture properties
    if (AI_SUCCESS == material->GetTexture(type, i, &path, &mapping, &uvindex,
                                           &blend, &op, &mapmode))
    {
      vbos.push_back(glLoadTexture(
          (dir + std::string("/") + path.C_Str()).c_str(), texture));
    }
    else // error managment
    {
      std::cerr << "\033[1;31;47mError, can't load texture from material: "
                << getName() << "\033[0m" << std::endl;

      exit(0);
    }
  }
  return vbos;
}

GLuint Material::glLoadTexture(std::string fileName, unsigned int texture)
{
  GLuint tex;
  // create vbo
  glGenTextures(1, &tex);
  // unbind texture
  glActiveTexture(GL_TEXTURE0 + texture);
  glBindTexture(GL_TEXTURE_2D, tex);

  // setup
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture

  // Texture image data
  int imgWidth, imgHeight;

  // Load the texture image
  unsigned char *image = SOIL_load_image(fileName.c_str(), &imgWidth,
                                         &imgHeight, 0, SOIL_LOAD_RGB);
  if (image == NULL) // error managment
  {
    std::cerr << "\033[1;31;47mError, can't load texture file : "
              << fileName.c_str() << "\033[0m" << std::endl;

    exit(0);
  }
  // flip the texture on Y axis
  unsigned char *flipped = new unsigned char[imgWidth * imgHeight * 3];
  for (int x = 0; x < imgWidth; x++)
  {
    for (int y = 0; y < imgHeight; y++)
    {
      flipped[((imgWidth - x - 1) * imgHeight + y) * 3] =
          image[(x * imgHeight + y) * 3];
      flipped[((imgWidth - x - 1) * imgHeight + y) * 3 + 1] =
          image[(x * imgHeight + y) * 3 + 1];
      flipped[((imgWidth - x - 1) * imgHeight + y) * 3 + 2] =
          image[(x * imgHeight + y) * 3 + 2];
    }
  }
  // load texture
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imgWidth, imgHeight, 0, GL_RGB,
               GL_UNSIGNED_BYTE, flipped);
  glGenerateMipmap(GL_TEXTURE_2D);

  // free image data.
  delete[] flipped;
  SOIL_free_image_data(image);

  // unbind texture
  glBindTexture(GL_TEXTURE_2D, 0);
  // return vbo
  return tex;
}

void Material::glSetFloatProperty(ShaderProgram shaderProgram,
                                  std::string uniformName, const char *pKey,
                                  unsigned int type, unsigned int idx)
{
  float value;
  GLuint uniformProperty =
      glGetUniformLocation(shaderProgram.getProgramId(), uniformName.c_str());
  if (AI_SUCCESS == material->Get(pKey, type, idx, value))
  {
    glUniform1f(uniformProperty, value);
  }
  else
  {
    glUniform1f(uniformProperty, 1);
    /*std::cerr << "\033[1;31;47mError, can't set property \"" << pKey
              << "\" of material \"" << getName() << "\"\033[0m" << std::endl;
    exit(0);*/
  }
}

void Material::glSetColorProperty(ShaderProgram shaderProgram,
                                  std::string uniformName, const char *pKey,
                                  unsigned int type, unsigned int idx)
{
  aiColor3D color;
  GLuint uniformProperty =
      glGetUniformLocation(shaderProgram.getProgramId(), uniformName.c_str());
  if (AI_SUCCESS == material->Get(pKey, type, idx, color))
  {
    glUniform3f(uniformProperty, color.r, color.g, color.b);
  }
  else
  {
    glUniform3f(uniformProperty, 0, 0, 0);
    /*std::cerr << "\033[1;31;47mError, can't set property \"" << pKey
              << "\" of material \"" << getName() << "\"\033[0m" << std::endl;
    exit(0);*/
  }
}

void Material::showMaterialInformation(const aiMaterial *pMaterial)
{
  aiString name;
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_NAME, name))
  {
    std::cout << "   Name: " << name.data << std::endl;
  }
  aiColor3D color;
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_AMBIENT, color))
  {
    std::cout << "   Ambient color: (" << color.r << ", " << color.g << ", "
              << color.b << ")" << std::endl;
  }
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color))
  {
    std::cout << "   Diffuse color: (" << color.r << ", " << color.g << ", "
              << color.b << ")" << std::endl;
  }
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_EMISSIVE, color))
  {
    std::cout << "   Emissive color: (" << color.r << ", " << color.g << ", "
              << color.b << ")" << std::endl;
  }
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_REFLECTIVE, color))
  {
    std::cout << "   Reflective color: (" << color.r << ", " << color.g << ", "
              << color.b << ")" << std::endl;
  }
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_SPECULAR, color))
  {
    std::cout << "   Specular color: (" << color.r << ", " << color.g << ", "
              << color.b << ")" << std::endl;
  }
  float value;
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_SHININESS, value))
  {
    std::cout << "   Shininess: " << value << std::endl;
  }
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_SHININESS_STRENGTH, value))
  {
    std::cout << "   Shininess strength: " << value << std::endl;
  }
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_TRANSPARENT, color))
  {
    std::cout << "   Transparent color: (" << color.r << ", " << color.g << ", "
              << color.b << ")" << std::endl;
  }
  int intValue;
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_ENABLE_WIREFRAME, intValue))
  {
    if (intValue == 0)
    {
      std::cout << "   Wireframe: Disabled" << std::endl;
    }
    else if (intValue == 1)
    {
      std::cout << "   Wireframe: Enabled" << std::endl;
    }
    else
    {
      std::cout << "   Wireframe: Unexpected value" << std::endl;
    }
  }
  if (AI_SUCCESS == pMaterial->Get(AI_MATKEY_SHADING_MODEL, intValue))
  {
    std::cout << "   Shading model: " << intValue << std::endl;
  }
  unsigned int aux = pMaterial->GetTextureCount(aiTextureType_AMBIENT);
  if (aux > 0)
  {
    std::cout << "   Number of ambient textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_AMBIENT, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_DIFFUSE);
  if (aux > 0)
  {
    std::cout << "   Number of diffuse textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_DIFFUSE, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_DISPLACEMENT);
  if (aux > 0)
  {
    std::cout << "   Number of displacement textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_DISPLACEMENT, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_EMISSIVE);
  if (aux > 0)
  {
    std::cout << "   Number of emissive textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_EMISSIVE, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_HEIGHT);
  if (aux > 0)
  {
    std::cout << "   Number of height textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_HEIGHT, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_LIGHTMAP);
  if (aux > 0)
  {
    std::cout << "   Number of lightmap textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_LIGHTMAP, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_NORMALS);
  if (aux > 0)
  {
    std::cout << "   Number of normals textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_NORMALS, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_OPACITY);
  if (aux > 0)
  {
    std::cout << "   Number of opacity textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_OPACITY, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_REFLECTION);
  if (aux > 0)
  {
    std::cout << "   Number of reflection textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_REFLECTION, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_SHININESS);
  if (aux > 0)
  {
    std::cout << "   Number of shininess textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_SHININESS, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_SPECULAR);
  if (aux > 0)
  {
    std::cout << "   Number of specular textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_SPECULAR, i);
    }
  }
  aux = pMaterial->GetTextureCount(aiTextureType_UNKNOWN);
  if (aux > 0)
  {
    std::cout << "   Number of unknown textures: " << aux << std::endl;
    for (unsigned int i = 0; i < aux; i++)
    {
      showTextureInformation(pMaterial, aiTextureType_UNKNOWN, i);
    }
  }
}

void Material::showTextureInformation(const aiMaterial *pMaterial,
                                      aiTextureType pType,
                                      unsigned int pTextureNumber)
{
  aiString path;
  aiTextureMapping mapping;
  unsigned int uvindex;
  float blend;
  aiTextureOp op;
  aiTextureMapMode mapmode;
  std::cout << "      Information of texture " << pTextureNumber << std::endl;
  if (AI_SUCCESS == pMaterial->GetTexture(pType, pTextureNumber, &path,
                                          &mapping, &uvindex, &blend, &op,
                                          &mapmode))
  {
    std::cout << "         Path: " << path.data << std::endl;
    std::cout << "         UV index: " << uvindex << std::endl;
    std::cout << "         Blend: " << blend << std::endl;
  }
  else
  {
    std::cout << "         Impossible to get the texture" << std::endl;
  }
}
