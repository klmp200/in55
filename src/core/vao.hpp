#ifndef VAO_H
#define VAO_H

#include <GL/glew.h>
#include <vector>

class Vao
{
public:
    Vao(){};
    Vao(GLuint id,std::vector<GLuint> bufferIds, int size);
    GLuint id;
    std::vector<GLuint> bufferIds;
    int size;
};

#endif