#ifndef __SHADER_CONFIG__
#define __SHADER_CONFIG__

// model
#define SHADER_VERTEX_NAME "vertex"
#define SHADER_NORMAL_NAME "normal"
#define SHADER_UV_NAME "textCoords"

// transformation
#define SHADER_VIEW_PROJECTION_MATRIX_NAME "vp"
#define SHADER_MODEL_MATRIX_NAME "model"
#define SHADER_BONE_TRANSFORMS_ARRAY_NAME "boneTransforms"

// materials property
#define SHADER_COLOR_AMBIANT_NAME "colorAmbient"
#define SHADER_COLOR_DIFFUSE_NAME "colorDiffuse"
#define SHADER_COLOR_EMISSIVE_NAME "colorEmissive"
#define SHADER_COLOR_REFLECTIVE_NAME "colorReflective"
#define SHADER_COLOR_SPECULAR_NAME "colorSpecular"
#define SHADER_SHININESS_NAME "shininess"
#define SHADER_SHININESS_STRENGTH_NAME "shininessStrength"
#define SHADER_TRANSPARENT_NAME "colorTransparent"

#define SHADER_TEXTURE_AMBIENT_NAME "samplerTextureAmbient"
#define SHADER_TEXTURE_DIFFUSE_NAME "samplerTextureDiffuse"
#define SHADER_TEXTURE_DISPLACEMENT_NAME "samplerTextureDisplacement"
#define SHADER_TEXTURE_EMISSIVE_NAME "samplerTextureEmissive"
#define SHADER_TEXTURE_HEIGHT_NAME "samplerTextureHeight"
#define SHADER_TEXTURE_LIGHTMAP_NAME "samplerTextureLightmap"
#define SHADER_TEXTURE_NORMALS_NAME "samplerTextureNormals"
#define SHADER_TEXTURE_OPACITY_NAME "samplerTextureOpacity"
#define SHADER_TEXTURE_REFLEXION_NAME "samplerTextureReflextion"
#define SHADER_TEXTURE_SHININESS_NAME "samplerTextureShininess"
#define SHADER_TEXTURE_SPECULAR_NAME "samplerTextureSpecular"
#define SHADER_TEXTURE_UNKNOWN_NAME "samplerTextureUnknown"

#define SHADER_HAS_TEXTURE_AMBIENT_NAME "hasTextureAmbient"
#define SHADER_HAS_TEXTURE_DIFFUSE_NAME "hasTextureDiffuse"
#define SHADER_HAS_TEXTURE_DISPLACEMENT_NAME "hasTextureDisplacement"
#define SHADER_HAS_TEXTURE_EMISSIVE_NAME "hasTextureEmissive"
#define SHADER_HAS_TEXTURE_HEIGHT_NAME "hasTextureHeight"
#define SHADER_HAS_TEXTURE_LIGHTMAP_NAME "hasTextureLightmap"
#define SHADER_HAS_TEXTURE_NORMALS_NAME "hasTextureNormals"
#define SHADER_HAS_TEXTURE_OPACITY_NAME "hasTextureOpacity"
#define SHADER_HAS_TEXTURE_REFLEXION_NAME "hasTextureReflextion"
#define SHADER_HAS_TEXTURE_SHININESS_NAME "hasTextureShininess"
#define SHADER_HAS_TEXTURE_SPECULAR_NAME "hasTextureSpecular"
#define SHADER_HAS_TEXTURE_UNKNOWN_NAME "hasTextureUnknown"

// camera
#define SHADER_CAMERA_POSITION_NAME "cameraPosition"

// lights
#define SHADER_LIGHT_ARRAY_NAME "lights"
#define SHADER_LIGHT_ARRAY_SIZE_NAME "lightsCount"
#define SHADER_LIGHT_ARRAY_MAX_SIZE 3
#define SHADER_LIGHT_TYPE_NAME "type"
#define SHADER_LIGHT_POSITION_NAME "position"
#define SHADER_LIGHT_DIRECTION_NAME "direction"
#define SHADER_LIGHT_ATTENUATION_NAME "attenuation"
#define SHADER_LIGHT_COLOR_NAME "color"

#define SHADER_LIGHT_AMBIANT_ENABLE_NAME "enableAmbiantLight"
#define SHADER_LIGHT_DIFFUSE_ENABLE_NAME "enableDiffuseLight"
#define SHADER_LIGHT_SPECULAR_ENABLE_NAME "enableSpecularLight"

#endif /* __SHADER_CONFIG__ */