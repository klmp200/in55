#ifndef __MATERIAL_H__
#define __MATERIAL_H__

// glew
#include <GL/glew.h>

// assimp
#include <assimp/material.h>

// glm
#include <glm/vec3.hpp>

// standard cpp
#include <string>
#include <vector>

// project
#include "shaderProgram.hpp"

class Material
{
public:
  Material();
  Material(const aiMaterial *_material, std::string dir = "");
  std::string getName(); // get the material's name
  void glBind();         // create vbo and bind it
  void
  glActivate(ShaderProgram shaderProgram); // activate the usage of the material
  const aiMaterial *getMaterial();

  static void showMaterialInformation(const aiMaterial *pMaterial);
  static void showTextureInformation(const aiMaterial *pMaterial,
                                     aiTextureType pType,
                                     unsigned int pTextureNumber);

  ~Material();

private:
  std::vector<GLuint> glLoadTextureType(std::string dir,
                                        enum aiTextureType type,
                                        unsigned int texture = 0);
  GLuint glLoadTexture(std::string fileName, unsigned int texture = 0);
  void glSetFloatProperty(ShaderProgram shaderProgram, std::string uniformName,
                          const char *pKey, unsigned int type,
                          unsigned int idx);
  void glSetColorProperty(ShaderProgram shaderProgram, std::string uniformName,
                          const char *pKey, unsigned int type,
                          unsigned int idx);

  std::string path;

  const aiMaterial *material;

  std::vector<GLuint> samplerTextureAmbient;
  std::vector<GLuint> samplerTextureDiffuse;
  std::vector<GLuint> samplerTextureDisplacement;
  std::vector<GLuint> samplerTextureEmissive;
  std::vector<GLuint> samplerTextureHeight;
  std::vector<GLuint> samplerTextureLightmap;
  std::vector<GLuint> samplerTextureNormals;
  std::vector<GLuint> samplerTextureOpacity;
  std::vector<GLuint> samplerTextureReflextion;
  std::vector<GLuint> samplerTextureShininess;
  std::vector<GLuint> samplerTextureSpecular;
  std::vector<GLuint> samplerTextureUnknown;
};

#endif /* __MATERIAL_H__ */