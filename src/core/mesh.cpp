#include <iostream>
#include <math.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "mesh.hpp"
#include "shader_config.hpp"

Mesh::Mesh(aiMesh *assimpMesh, std::vector<Material> &materials, const aiMatrix4x4& globalInverseTransform)
{
  this->globalInverseTransform = this->aiMatrix4x4ToGlm(&globalInverseTransform);
  this->assimpMesh = assimpMesh;
  this->material = materials[assimpMesh->mMaterialIndex];

  GLuint vaoId;
  glGenVertexArrays(1, &vaoId);
  this->vao = Vao(vaoId, std::vector<GLuint>(),
                  assimpMesh->mNumFaces * 3 * sizeof(unsigned int));

  this->transform = glm::mat4(1.0f);

  this->name = std::string(assimpMesh->mName.C_Str());

  //loading bone datas
  for (int i = 0; i < assimpMesh->mNumBones; i++){
    this->boneNames.push_back(std::string(assimpMesh->mBones[i]->mName.C_Str()));
    for (int j = 0; j < assimpMesh->mBones[i]->mNumWeights; j++){
      this->addBoneVertexData(
        assimpMesh->mBones[i]->mWeights[j].mVertexId,
        i,
        assimpMesh->mBones[i]->mWeights[j].mWeight
      );
    }
  }
}

Mesh::~Mesh() {}

void Mesh::addBoneVertexData(unsigned int vertexId, unsigned int boneId, float weight){
  //if not already in map : insert
  if (this->vertexToBoneData.find(vertexId) == this->vertexToBoneData.end()){
    BoneVertexData boneVertexData;
    boneVertexData.boneIds[0] = boneId;
    boneVertexData.weights[0] = weight;
    for (int i = 1; i < MAX_BONES_PER_VERTEX; i++){
      boneVertexData.weights[i] = 0;
    }
    this->vertexToBoneData[vertexId] = boneVertexData;
    return;
  }

  //if in map : find first available place
  for (int i = 0; i < MAX_BONES_PER_VERTEX; i++){
    if (this->vertexToBoneData[vertexId].weights[i] == 0.0){
      this->vertexToBoneData[vertexId].weights[i] = weight;
      this->vertexToBoneData[vertexId].boneIds[i] = boneId;
      return;
    }
  }

  //if map is full : try to replace lowest value
  int lowestWeightIndex = 0;
  float lowestWeight = this->vertexToBoneData[vertexId].weights[0];
  for (int i = 1; i < MAX_BONES_PER_VERTEX; i++){
    if (this->vertexToBoneData[vertexId].weights[i] < lowestWeight){
      lowestWeight = this->vertexToBoneData[vertexId].weights[i];
      lowestWeightIndex = i;
    }
  }

  if (weight > lowestWeight){
    this->vertexToBoneData[vertexId].weights[lowestWeightIndex] = weight;
    this->vertexToBoneData[vertexId].boneIds[lowestWeightIndex] = boneId;
  }
}

void Mesh::glLoad()
{

  unsigned int indices[assimpMesh->mNumFaces * 3];
  for (unsigned int t = 0; t < assimpMesh->mNumFaces; ++t)
  {
    const aiFace *face = &assimpMesh->mFaces[t];
    indices[t * 3] = face->mIndices[0];
    indices[t * 3 + 1] = face->mIndices[1];
    indices[t * 3 + 2] = face->mIndices[2];
  }

  float textCoords[assimpMesh->mNumVertices * 2];
  if (assimpMesh->mTextureCoords[0] != NULL)
  {
    for (unsigned int t = 0; t < assimpMesh->mNumVertices; ++t)
    {
      textCoords[t * 2] = assimpMesh->mTextureCoords[0][t].x;
      textCoords[t * 2 + 1] = assimpMesh->mTextureCoords[0][t].y;
    }
  }

  GLuint vbo; // vertices
  GLuint ibo; // indices
  GLuint nbo; // normal
  GLuint tbo; // text coords
  GLuint tex; // 2D textures
  GLuint wbo; // bone weights
  GLuint bbo; // bone ids

  unsigned int indicesSize = assimpMesh->mNumFaces * 3 * sizeof(unsigned int);
  unsigned int verticesSize = assimpMesh->mNumVertices * 3 * sizeof(float);
  unsigned int normalSize = assimpMesh->mNumVertices * 3 * sizeof(float);
  unsigned int textCoordsSize = assimpMesh->mNumVertices * 2 * sizeof(float);

  glBindVertexArray(this->vao.id);

  // create vbo for vertices
  glGenBuffers(1, &vbo);              // create 1 buffer
  glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind it

  glBufferData(GL_ARRAY_BUFFER, verticesSize, assimpMesh->mVertices,
               GL_STATIC_DRAW); // copy data into gpu memory

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);

  // create vbo for normals
  glGenBuffers(1, &nbo);              // create 1 buffer
  glBindBuffer(GL_ARRAY_BUFFER, nbo); // bind it

  glBufferData(GL_ARRAY_BUFFER, normalSize, assimpMesh->mNormals,
               GL_STATIC_DRAW); // copy data into gpu memory

  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(1);

  // create vbo for textures coords
  glGenBuffers(1, &tbo);              // create 1 buffer
  glBindBuffer(GL_ARRAY_BUFFER, tbo); // bind it

  glBufferData(GL_ARRAY_BUFFER, textCoordsSize,
               textCoords,      // mesh->mTextureCoords,
               GL_STATIC_DRAW); // copy data into gpu memory

  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(2);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // create vbo for bone weights
  glGenBuffers(1, &wbo);
  glBindBuffer(GL_ARRAY_BUFFER, wbo);

  int boneWeightDataSize = this->assimpMesh->mNumVertices * MAX_BONES_PER_VERTEX;
  float weights[boneWeightDataSize];
  for (int i = 0; i < boneWeightDataSize; i ++){
    weights[i] = 0;
  }
  glBufferData(GL_ARRAY_BUFFER, boneWeightDataSize * sizeof(float), weights, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(3, MAX_BONES_PER_VERTEX, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(3);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // create vbo for bone ids
  glGenBuffers(1, &bbo);
  glBindBuffer(GL_ARRAY_BUFFER, bbo);
  int boneIdDataSize = this->assimpMesh->mNumVertices * MAX_BONES_PER_VERTEX;
  int boneIds[boneIdDataSize];
  for (int i = 0; i < boneIdDataSize; i++){
    boneIds[i] = -1;
  }
  glBufferData(GL_ARRAY_BUFFER, boneIdDataSize * sizeof(int), boneIds, GL_DYNAMIC_DRAW);
  glVertexAttribIPointer(4, MAX_BONES_PER_VERTEX, GL_INT, 0, 0);
  glEnableVertexAttribArray(4);

  // textures
  this->material.glBind();

  // indices buffer
  glGenBuffers(1, &ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize, &indices[0],
               GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  vao.bufferIds.push_back(vbo);
  vao.bufferIds.push_back(ibo);
  vao.bufferIds.push_back(nbo);
  vao.bufferIds.push_back(tbo);
  vao.bufferIds.push_back(tex);

  vao.bufferIds.push_back(wbo);
  vao.bufferIds.push_back(bbo);
  this->boneWeightsBuffer = wbo;
  this->boneIdsBuffer = bbo;
}

void Mesh::playAnimation(const aiScene* scene, const ShaderProgram& shaderProgram, float time){
  if (this->boneNames.empty()){
    return;
  }
  if (scene->mNumAnimations < 1){
    return;
  }
  aiAnimation* animation = scene->mAnimations[0];

  int minDepth = this->getNodeDepth(scene->mRootNode, this->boneNames[0], 0);
  std::string minDepthName = this->boneNames[0];
  for (auto boneName : this->boneNames){
    int depth = this->getNodeDepth(scene->mRootNode, boneName, 0);
    if (depth < minDepth){
      minDepth = depth;
      minDepthName = boneName;
    }
  }
  if (minDepth == -1){
    std::cout << "[warning] could not find animation root node" << std::endl;
    return;
  }
  aiNode* rootNode = scene->mRootNode->FindNode(aiString(minDepthName));

  std::map<std::string, glm::mat4> nameToTransform;
  for (auto boneName : this->boneNames){
    aiBone* bone = this->getBoneByName(boneName);
    nameToTransform[boneName] = this->aiMatrix4x4ToGlm(&bone->mOffsetMatrix);
    //nameToTransform[boneName] = glm::mat4(1.0f);
  }
  this->updateBonesTransforms(nameToTransform, rootNode, animation, time);

  /*for (auto boneName : this->boneNames){
    nameToTransform[boneName] = this->globalInverseTransform * nameToTransform[boneName];
  }*/

  int boneShaderDataSize = MAX_BONES_PER_VERTEX * this->assimpMesh->mNumVertices;
  float weight[boneShaderDataSize];
  for (int i = 0; i < boneShaderDataSize; i++){
    weight[i] = 0;
  }
  int boneIds[boneShaderDataSize];


  for (int i = 0; i < this->assimpMesh->mNumVertices; i++){
    for (int j = 0; j < MAX_BONES_PER_VERTEX; j++){
      weight[i * MAX_BONES_PER_VERTEX + j] = this->vertexToBoneData[i].weights[j];
      boneIds[i * MAX_BONES_PER_VERTEX + j] = this->vertexToBoneData[i].boneIds[j];
    }

    //normalize weight
    float vertexWeightSum = 0;
    for (int j = 0; j < MAX_BONES_PER_VERTEX; j++){
      vertexWeightSum += weight[i * MAX_BONES_PER_VERTEX + j]; 
    }
    if (vertexWeightSum == 0){
      continue;
    }
    for (int j = 0; j < MAX_BONES_PER_VERTEX; j++){
      weight[i * MAX_BONES_PER_VERTEX + j] = weight[i * MAX_BONES_PER_VERTEX + j] * (1 / vertexWeightSum);
    }
  }

  glm::mat4 transforms[this->assimpMesh->mNumBones];
  for (int i = 0; i < this->assimpMesh->mNumBones; i++){
    transforms[i] = nameToTransform[this->boneNames[i]];
  }

  glBindVertexArray(this->vao.id);
  //update weights and bones ids buffers
  glBindBuffer(GL_ARRAY_BUFFER, this->boneWeightsBuffer);
  glBufferSubData(GL_ARRAY_BUFFER, 0, boneShaderDataSize * sizeof(float), weight);
  glBindBuffer(GL_ARRAY_BUFFER, this->boneIdsBuffer);
  glBufferSubData(GL_ARRAY_BUFFER, 0, boneShaderDataSize * sizeof(int), boneIds);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  GLint boneTransformsId = glGetUniformLocation(shaderProgram.getProgramId(), SHADER_BONE_TRANSFORMS_ARRAY_NAME);
  glUniformMatrix4fv(boneTransformsId, this->assimpMesh->mNumBones, GL_FALSE, glm::value_ptr(transforms[0]));

  glBindVertexArray(0);

}

void Mesh::updateBonesTransforms(std::map<std::string,glm::mat4>& transforms, const aiNode* node, const aiAnimation* anim, float time){
  aiNodeAnim* currentAnim = this->getNodeAnimByName(anim, node->mName);
  if (currentAnim == nullptr){
    std::cout << "got null animation while updating bone transforms" << std::endl;
  }

  glm::quat rotation = this->interpolateRotation(time, currentAnim);
  glm::mat4 rotationMatrix = glm::toMat4(rotation);

  glm::vec3 translation = this->interpolateTranslation(time, currentAnim); 
  glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f),translation);

  glm::vec3 scale = this->interpolateScale(time, currentAnim);
  glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), scale);

  //bone offset matrix
  aiMatrix4x4 aiOffsetMatrix = this->getBoneByName(std::string(node->mName.C_Str()))->mOffsetMatrix;
  glm::mat4 offsetMatrix = this->aiMatrix4x4ToGlm(&aiOffsetMatrix);

  glm::mat4 nodeTransform = translationMatrix * rotationMatrix * scaleMatrix;
  glm::mat4 globalTransform = transforms[std::string(node->mName.C_Str())] * nodeTransform;

  transforms[std::string(node->mName.C_Str())] =
      globalTransform *
      offsetMatrix;

  for (int i = 0; i < node->mNumChildren; i++){
    if (transforms.count(std::string(node->mChildren[i]->mName.C_Str())) > 0){
      transforms[std::string(node->mChildren[i]->mName.C_Str())] = globalTransform;
    }
  }

  for (int i = 0; i < node->mNumChildren; i++){
    if (transforms.count(std::string(node->mChildren[i]->mName.C_Str())) > 0){
      updateBonesTransforms(transforms, node->mChildren[i], anim, time);
    }
  }
}

glm::quat Mesh::interpolateRotation(float time, const aiNodeAnim* nodeAnim){
  if (nodeAnim->mNumRotationKeys == 1){
    aiQuaternion out = nodeAnim->mRotationKeys[0].mValue;
    glm::quat rotation(out.w ,out.x, out.y, out.z);
    return rotation;
  }
  time = fmod(time, nodeAnim->mRotationKeys[nodeAnim->mNumRotationKeys - 1].mTime);
  int lastKey;
  for (int i = 0; i < nodeAnim->mNumRotationKeys - 1; i++){
    if (time < (float) nodeAnim->mRotationKeys[i+1].mTime){
      lastKey = i;
      break;
    }
  }
  //if we are before or after animation start, return identity quaternion
  if (time <= (float) nodeAnim->mRotationKeys[lastKey].mTime){ 
    aiQuaternion out = nodeAnim->mRotationKeys[0].mValue;
    glm::quat rotation(out.w ,out.x, out.y, out.z);
    return rotation;
  } else if (time > (float) nodeAnim->mRotationKeys[nodeAnim->mNumRotationKeys - 1].mTime){
    aiQuaternion out = nodeAnim->mRotationKeys[nodeAnim->mNumRotationKeys - 1].mValue;
    glm::quat rotation(out.w ,out.x, out.y, out.z);
    return rotation;
  }

  int nextKey = lastKey + 1;

  float deltaTime = nodeAnim->mRotationKeys[nextKey].mTime - nodeAnim->mRotationKeys[lastKey].mTime;
  float factor = (time - nodeAnim->mRotationKeys[lastKey].mTime) / deltaTime;
  const aiQuaternion& startQuaternion = nodeAnim->mRotationKeys[lastKey].mValue;
  const aiQuaternion& endQuaternion = nodeAnim->mRotationKeys[nextKey].mValue;
  aiQuaternion out;
  aiQuaternion::Interpolate(out, startQuaternion, endQuaternion, factor);
  out.Normalize();
  glm::quat rotation(out.w, out.x, out.y, out.z);
  return rotation;
}

glm::vec3 Mesh::interpolateTranslation(float time, const aiNodeAnim* nodeAnim){
  if (nodeAnim->mNumPositionKeys == 1){
    aiVector3D out = nodeAnim->mPositionKeys[0].mValue;
    glm::vec3 translation = glm::vec3(out.x, out.y, out.z);
    return translation;
  }
  time = fmod(time, nodeAnim->mPositionKeys[nodeAnim->mNumPositionKeys - 1].mTime);
  int lastKey;
  for (int i = 0; i < nodeAnim->mNumPositionKeys - 1; i++){
    if (time < (float) nodeAnim->mPositionKeys[i+1].mTime){
      lastKey = i;
      break;
    }
  }
  if (time <= (float) nodeAnim->mPositionKeys[lastKey].mTime){
    aiVector3D out = nodeAnim->mPositionKeys[0].mValue;
    glm::vec3 translation = glm::vec3(out.x, out.y, out.z);
    return translation;
  } else if (time > (float) nodeAnim->mPositionKeys[nodeAnim->mNumPositionKeys - 1].mTime){
    aiVector3D out = nodeAnim->mPositionKeys[nodeAnim->mNumPositionKeys].mValue;
    glm::vec3 translation = glm::vec3(out.x, out.y, out.z);
    return translation;
  }
 
  int nextKey = lastKey + 1;

  float deltaTime = nodeAnim->mPositionKeys[nextKey].mTime - nodeAnim->mPositionKeys[lastKey].mTime;
  float firstFactor = (nodeAnim->mPositionKeys[nextKey].mTime - time) / (deltaTime);
  float endFactor = (time - nodeAnim->mPositionKeys[lastKey].mTime) / (deltaTime);

  aiVector3D out = firstFactor * nodeAnim->mPositionKeys[lastKey].mValue
    + endFactor * nodeAnim->mPositionKeys[nextKey].mValue;

  glm::vec3 translation = glm::vec3(out.x, out.y, out.z);
  return translation;
}

glm::vec3 Mesh::interpolateScale(float time, const aiNodeAnim* nodeAnim){
  if (nodeAnim->mNumScalingKeys == 1){
    aiVector3D out = nodeAnim->mScalingKeys[0].mValue;
    glm::vec3 scale = glm::vec3(out.x, out.y, out.z);
    return scale;
  }
  int lastKey;
  for (int i = 0; i < nodeAnim->mNumScalingKeys - 1; i++){
    if (time < (float) nodeAnim->mScalingKeys[i+1].mTime){
      lastKey = i;
      break;
    }
  }
  time = fmod(time, nodeAnim->mScalingKeys[nodeAnim->mNumScalingKeys - 1].mTime);
  if (time <= (float) nodeAnim->mScalingKeys[lastKey].mTime) {
    aiVector3D out = nodeAnim->mScalingKeys[0].mValue;
    glm::vec3 scale = glm::vec3(out.x, out.y, out.z);
    return scale;
  } else if (time > (float) nodeAnim->mScalingKeys[nodeAnim->mNumScalingKeys - 1].mTime){
    aiVector3D out = nodeAnim->mScalingKeys[nodeAnim->mNumScalingKeys - 1].mValue;
    glm::vec3 scale = glm::vec3(out.x, out.y, out.z);
    return scale;
  }

  int nextKey = lastKey + 1;

  float deltaTime = nodeAnim->mScalingKeys[nextKey].mTime - nodeAnim->mScalingKeys[lastKey].mTime;
  float firstFactor = (nodeAnim->mScalingKeys[nextKey].mTime - time) / (deltaTime);
  float endFactor = (time - nodeAnim->mScalingKeys[lastKey].mTime) / (deltaTime);
  aiVector3D out = firstFactor * nodeAnim->mScalingKeys[lastKey].mValue
    + endFactor * nodeAnim->mScalingKeys[nextKey].mValue;
  glm::vec3 scale = glm::vec3(out.x, out.y, out.z);
  return scale;
}


aiNodeAnim* Mesh::getNodeAnimByName(const aiAnimation* anim, const aiString name){
  for (int i = 0; i < anim->mNumChannels; i++){
    if (anim->mChannels[i]->mNodeName == name){
      return anim->mChannels[i];
    }
  }
  return nullptr;
}

aiBone* Mesh::getBoneByName(std::string name){
  for (int i = 0; i < this->assimpMesh->mNumBones; i++){
    if (std::string(this->assimpMesh->mBones[i]->mName.C_Str()) == name){
      return this->assimpMesh->mBones[i];
    }
  }
  std::cout << "[warning] : could not find bone by his name : " << name << std::endl;
  return nullptr;
}
  
glm::mat4 Mesh::aiMatrix4x4ToGlm(const aiMatrix4x4* from){
    glm::mat4 to;
    to[0][0] = (GLfloat)from->a1; to[0][1] = (GLfloat)from->b1;  to[0][2] = (GLfloat)from->c1; to[0][3] = (GLfloat)from->d1;
    to[1][0] = (GLfloat)from->a2; to[1][1] = (GLfloat)from->b2;  to[1][2] = (GLfloat)from->c2; to[1][3] = (GLfloat)from->d2;
    to[2][0] = (GLfloat)from->a3; to[2][1] = (GLfloat)from->b3;  to[2][2] = (GLfloat)from->c3; to[2][3] = (GLfloat)from->d3;
    to[3][0] = (GLfloat)from->a4; to[3][1] = (GLfloat)from->b4;  to[3][2] = (GLfloat)from->c4; to[3][3] = (GLfloat)from->d4;
    return to;
}

/**
 * getNodeDepth
 * @param rootNode
 * @param name
 * @param currentDepth
 * @returns -1 if nothing was found, minimum depth of the node corresponding to the depth otherwise
 */
int Mesh::getNodeDepth(const aiNode* rootNode, std::string name, int currentDepth){
  if (std::string(rootNode->mName.C_Str()) == name){
    return currentDepth;
  }
  if (rootNode->mNumChildren == 0){
    return -1;
  }

  int minDepth = getNodeDepth(rootNode->mChildren[0], name, currentDepth + 1);
  if (minDepth != -1){
    return minDepth;
  }
  for (int i = 0; i < rootNode->mNumChildren; i++){
    int depth = getNodeDepth(rootNode->mChildren[i], name, currentDepth + 1);
    if (depth != -1){
      return depth;
    }
    if (minDepth == -1 || (depth < minDepth && depth != -1)){
      minDepth = depth;
    }
  }
  return minDepth;
}

void Mesh::glDraw(ShaderProgram program)
{
  GLint transformId =
      glGetUniformLocation(program.getProgramId(), SHADER_MODEL_MATRIX_NAME);
  glUniformMatrix4fv(transformId, 1, GL_FALSE, &this->transform[0][0]);

  glBindVertexArray(this->vao.id);
  this->material.glActivate(program);
  glEnableVertexAttribArray(0);
  glDrawElements(GL_TRIANGLES, this->vao.size, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
}
void Mesh::setTransform(glm::mat4 &transform)
{
  this->transform = transform;
}

void Mesh::translate(const glm::vec3 &translation)
{
  this->transform = glm::translate(this->transform, translation);
  std::cout << glm::to_string(this->transform) << std::endl;
}


BoundingBox Mesh::calculateBoundingBox()
{
  BoundingBox bb;
  glm::vec3 vertex(assimpMesh->mVertices[0].x, assimpMesh->mVertices[0].y, assimpMesh->mVertices[0].z);
  bb.min = vertex;
  bb.max = vertex;
  for (unsigned int i = 1; i < assimpMesh->mNumVertices; ++i)
  {
    vertex = glm::vec3(assimpMesh->mVertices[i].x, assimpMesh->mVertices[i].y, assimpMesh->mVertices[i].z);
    bb.min = glm::min(vertex, bb.min);
    bb.max = glm::max(vertex, bb.max);
  }
  return bb;
}