#include "shaderProgram.hpp"

#include "shader_config.hpp"

ShaderProgram::ShaderProgram() {}

ShaderProgram::ShaderProgram(const std::list<Shader> shaders) {
  this->programId = glCreateProgram();
  for (Shader shader : shaders) {
    glAttachShader(this->programId, shader.getShaderId());
    this->shaders.push_back(shader);
    // glDeleteShader(shaderId) ??
  }
  glLinkProgram(this->programId);
}

ShaderProgram::~ShaderProgram() {}

const GLuint ShaderProgram::getProgramId() const { return programId; }
