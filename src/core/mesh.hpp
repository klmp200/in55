#ifndef MESH_H
#define MESH_H

#include <assimp/scene.h>
#include <assimp/quaternion.h>

#include <glm/glm.hpp>

#include <vector>
#include <map>

#include "material.hpp"
#include "shaderProgram.hpp"
#include "vao.hpp"

#define MAX_BONES_PER_VERTEX 4

typedef struct
{
  glm::vec3 min;
  glm::vec3 max;
} BoundingBox;

class Mesh
{
  typedef struct
  {
    unsigned int boneIds[MAX_BONES_PER_VERTEX];
    float weights[MAX_BONES_PER_VERTEX];
  } BoneVertexData;

public:
  Mesh(aiMesh *assimpMesh, std::vector<Material> &materials, const aiMatrix4x4 &globalInverseTransform);
  ~Mesh();
  void glLoad();
  void glDraw(ShaderProgram program);

  void setTransform(glm::mat4 &transform);
  void translate(const glm::vec3 &translation);
  BoundingBox calculateBoundingBox();
  void playAnimation(const aiScene *scene, const ShaderProgram &shaderProgram, float time);
  // void rotateAround(const glm::vec3 target, const glm::vec3 axis, const float
  // angle); void scale(const float scale); void resetTransform();
private:
  void addBoneVertexData(unsigned int vertexId, unsigned int boneId, float weight);
  int getNodeDepth(const aiNode *rootNode, std::string name, int currentDepth);
  void updateBonesTransforms(
      std::map<std::string, glm::mat4> &transforms,
      const aiNode *node,
      const aiAnimation *anim,
      float time);
  glm::quat interpolateRotation(float time, const aiNodeAnim *nodeAnim);
  glm::vec3 interpolateTranslation(float time, const aiNodeAnim *nodeAnim);
  glm::vec3 interpolateScale(float time, const aiNodeAnim *nodeAnim);
  aiNodeAnim *getNodeAnimByName(const aiAnimation *anim, const aiString name);
  aiBone *getBoneByName(std::string name);
  glm::mat4 aiMatrix4x4ToGlm(const aiMatrix4x4 *from);

  std::string name;
  std::map<unsigned int, BoneVertexData> vertexToBoneData;
  std::vector<std::string> boneNames;
  GLuint boneWeightsBuffer;
  GLuint boneIdsBuffer;
  glm::mat4 transform;
  glm::mat4 globalInverseTransform;
  Vao vao;
  Material material;
  aiMesh *assimpMesh;
};

#endif