#ifndef __LIGHT_H__
#define __LIGHT_H__

// glew
#include <GL/glew.h>

// glm
#include <glm/gtc/type_ptr.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

// standard lib
#include <string>

// project
#include "shaderProgram.hpp"

class Light
{
public:
  Light();
  Light setAmbianteLight(glm::vec3 _color);
  Light setAmbianteLight(float power);
  Light setPointLight(glm::vec3 _position, glm::vec3 _color = glm::vec3(0.0),
                      glm::vec2 _attenuation = glm::vec2(0));
  Light setPointLight(glm::vec3 _position, float power = 0.0,
                      glm::vec2 _attenuation = glm::vec2(0));
  Light setDirectionalLight(glm::vec3 _direction,
                            glm::vec3 _color = glm::vec3(0.0));
  Light setDirectionalLight(glm::vec3 _direction, float power = 0.0);
  void glUse(ShaderProgram shader, unsigned int id);

  static void glSetLightCount(ShaderProgram shader, unsigned int count);
  static void glEnableLight(const ShaderProgram shader, bool enableAmbiantLight,
                            bool enableDiffuseLight, bool enableSpecularLight);

  glm::vec3 position;
  glm::vec3 direction;
  glm::vec2 attenuation;
  glm::vec3 color;

private:
  static GLuint getLightVarLoc(const ShaderProgram shader, unsigned int id,
                               std::string field);
  enum Type
  {
    AMBIANT = 0,
    POINT = 1,
    DIRECTIONAL = 2
  };
  enum Type type;
};

#endif /* __LIGHT_H__ */