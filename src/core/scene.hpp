#ifndef SCENE_H
#define SCENE_H

#include <GL/glew.h>

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <map>
#include <string>
#include <vector>

#include "material.hpp"
#include "shaderProgram.hpp"
#include "vao.hpp"
#include "mesh.hpp"
#include "light.hpp"

class Scene
{
public:
  Scene(std::string path);
  ~Scene();
  void glLoad();
  void glDraw(ShaderProgram program);
  void glUnload();
  void playAnimation(const ShaderProgram& shaderProgram, float time);
  int addLight(Light light);
  Light *getLight(int id);
  void removeLight(int id);
  unsigned int getLightCount();
  Mesh *getMesh(int id);
  unsigned int getMeshCount();
  BoundingBox calculateBoundingBox();

  void processAnimation(const float delta);

private:
  std::string fileName;
  const aiScene *assimpScene;
  Assimp::Importer importer;
  std::vector<Material> materials;
  std::vector<Mesh> meshes;
  std::vector<Light> lights;
  float animationDate = 0;
};

#endif