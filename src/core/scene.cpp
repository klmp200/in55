#include "scene.hpp"
#include <SOIL/SOIL.h>
#include <iostream>
// GLM includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <memory>

Scene::Scene(std::string path)
{
  fileName = path;
  this->assimpScene = this->importer.ReadFile(
      path.c_str(), aiProcess_Triangulate | aiProcess_GenNormals);
  if ((!assimpScene) || (assimpScene->mFlags == AI_SCENE_FLAGS_INCOMPLETE) ||
      (!assimpScene->mRootNode))
  {
    std::cerr << "\033[1;31;47mError loading model file : "
              << std::string(importer.GetErrorString()) << "\033[0m"
              << std::endl;

    exit(0);
  }
  // find file's directory
  std::size_t found = path.find_last_of("/");
  std::string directory = path.substr(0, found);
  // add all materials
  for (int i = 0; i < assimpScene->mNumMaterials; i++)
  {
    materials.push_back(Material(assimpScene->mMaterials[i], directory));
  }

  for (int i = 0; i < assimpScene->mNumMeshes; i++)
  {
    Mesh mesh(assimpScene->mMeshes[i], this->materials, assimpScene->mRootNode->mTransformation);
    this->meshes.push_back(mesh);
  }
}

Scene::~Scene() {}

void Scene::glLoad()
{
  for (auto &material : this->materials)
  {
    Material::showMaterialInformation(material.getMaterial());
  }

  for (auto &mesh : this->meshes)
  {
    mesh.glLoad();
  }

  //for (unsigned int i = 0; i < this->assimpScene->mAnimations[0]->mNumChannels; i++){
  //  std::cout << this->assimpScene->mAnimations[0]->mChannels[i]->mNodeName.C_Str() << std::endl;
  //}
}

void Scene::glDraw(ShaderProgram program)
{
  for (int i = 0; i < lights.size(); i++)
  {
    lights[i].glUse(program, i);
  }
  Light::glSetLightCount(program, lights.size());

  for (auto &mesh : this->meshes)
  {
    mesh.glDraw(program);
  }
}

void Scene::playAnimation(const ShaderProgram &shaderProgram, float time)
{
  for (auto &mesh : this->meshes)
  {
    mesh.playAnimation(this->assimpScene, shaderProgram, time);
  }
}

int Scene::addLight(Light light)
{
  lights.push_back(light);
  return lights.size() - 1;
}

Light *Scene::getLight(int id)
{
  return &lights[id];
}

void Scene::removeLight(int id)
{
  lights.erase(lights.begin() + id);
}
unsigned int Scene::getLightCount()
{
  return lights.size();
}

Mesh *Scene::getMesh(int id)
{
  return &meshes[id];
}

unsigned int Scene::getMeshCount()
{
  return meshes.size();
}

BoundingBox Scene::calculateBoundingBox()
{
  BoundingBox bb = meshes[0].calculateBoundingBox();

  for (int i = 1; i < this->meshes.size(); i++)
  {
    BoundingBox meshBb = meshes[i].calculateBoundingBox();
    bb.min = glm::min(bb.min, meshBb.min);
    bb.max = glm::max(bb.max, meshBb.max);
  }
  return bb;
}

void Scene::processAnimation(const float delta)
{
  if (this->assimpScene->mNumAnimations == 0)
  {
    std::cerr << "Error : no animation found" << std::endl;
    return;
  }
  for (unsigned int i = 0; i < this->assimpScene->mAnimations[0]->mNumChannels; i++)
  {
    aiNode *node = this->assimpScene->mRootNode->FindNode(this->assimpScene->mAnimations[0]->mChannels[i]->mNodeName);
    if (node != nullptr)
    {
    }
  }
}

// todo
void Scene::glUnload() {}
