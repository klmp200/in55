#define GLFW_INCLUDE_GL3
#define GLFW_NO_GLU

// GLEW include
#include <GL/glew.h>

// GLFW include
#include <GLFW/glfw3.h>

// GLM includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

// ASSIMP includes
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

// std includes
#include <fstream>
#include <iostream>
#include <list>
#include <string>
#include <sstream>
#include <iomanip>

// time
#include <chrono>
#include <ctime>

// local includes
#include "core/camera.hpp"
#include "core/light.hpp"
#include "core/scene.hpp"
#include "core/shader.hpp"
#include "core/shaderProgram.hpp"
#include "core/shader_config.hpp"

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768

typedef struct
{
  bool enableAmbiant;
  bool enableDiffuse;
  bool enableSpecular;
} LightConfig;

void APIENTRY openglCallbackFunction(GLenum source, GLenum type, GLuint id,
                                     GLenum severity, GLsizei length,
                                     const GLchar *message,
                                     const void *userParameters);
GLFWwindow *glInit();
void glDrawExp(GLFWwindow *window, Camera &activeCamera, std::vector<Scene *> scenes);
LightConfig updateLightControle(GLFWwindow *window, ShaderProgram &shader, LightConfig *light = NULL);
CameraRotateAround setupCamera(Scene &scene);

int main(int argc, char **argv)
{

  GLFWwindow *window = glInit();
  if (window == NULL)
    return 1;

  std::string fileName = "../resources/hand.obj";
  if (argc > 1)
  {
    fileName = argv[1];
  }
  Scene scene(fileName);
  scene.glLoad();
  Scene cube("../resources/cube.obj");
  cube.addLight(Light().setAmbianteLight(1.0));
  cube.glLoad();

  std::vector<Scene *> scenes;
  scenes.push_back(&scene);
  scenes.push_back(&cube);

  std::list<Shader> shaders;
  shaders.push_back(Shader("fragmentShader.frag", GL_FRAGMENT_SHADER));
  shaders.push_back(Shader("vertexShader.vert", GL_VERTEX_SHADER));

  CameraRotateAround camera = setupCamera(scene);

  scene.addLight(Light().setAmbianteLight(0.1));
  int pointLight1 = scene.addLight(Light().setPointLight(camera.getEye(), 0.3));
  float distance = glm::distance(camera.getEye(), camera.getCenter());

  ShaderProgram shaderProgram(shaders);
  camera.setShaderProgram(shaderProgram);

  auto start = std::chrono::system_clock::now();
  float total = 0;
  do
  {
    //time calculation
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    float duration = elapsed_seconds.count();
    total += duration;

    //show fps
    std::stringstream title;
    title << "[IN55] ©Zoo 2019 (" << std::fixed << std::setprecision(1) << (1.0 / duration) << "fps)";
    glfwSetWindowTitle(window, title.str().c_str());

    //make the cube move with the light
    glm::vec3 postion = scene.getLight(pointLight1)->position =
        camera.getCenter() + glm::vec3(glm::cos(total) * distance, distance / 3, glm::sin(total) * distance);
    glm::mat4 tranform = glm::translate(glm::mat4(1.0), postion);
    tranform = glm::scale(tranform, glm::vec3(distance / 20));
    cube.getMesh(0)->setTransform(tranform);

    //user input
    camera.handleInputs(window, duration);
    updateLightControle(window, shaderProgram);

    scene.playAnimation(camera.getShaderProgram(), total);

    //display
    glDrawExp(window, camera, scenes);

    start = end;
  } while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0);

  glfwTerminate();

  return 0;
}

void APIENTRY openglCallbackFunction(GLenum source, GLenum type, GLuint id,
                                     GLenum severity, GLsizei length,
                                     const GLchar *message,
                                     const void *userParameters)
{
  switch (type)
  {
  case GL_DEBUG_TYPE_ERROR:
    std::cout << "ERROR : ";
    break;
  case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
    std::cout << "DEPRECATED BEHAVIOUR : ";
    break;
  case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
    std::cout << "UNDEFINED BEHAVIOUR : ";
    break;
  case GL_DEBUG_TYPE_PORTABILITY:
    std::cout << "PORTABILITY : ";
    break;
  case GL_DEBUG_TYPE_PERFORMANCE:
    std::cout << "PERFORMANCE : ";
    break;
  case GL_DEBUG_TYPE_OTHER:
    std::cout << "OTHER : ";
    break;
  }

  std::cout << message;

  switch (severity)
  {
  case GL_DEBUG_SEVERITY_LOW:
    std::cout << " (SEVERITY : LOW)";
    break;
  case GL_DEBUG_SEVERITY_MEDIUM:
    std::cout << " (SEVERITY : MEDIUM)";
    break;
  case GL_DEBUG_SEVERITY_HIGH:
    std::cout << " (SEVERITY : HIGH)";
    break;
  case GL_DEBUG_SEVERITY_NOTIFICATION:
    std::cout << " (SEVERITY : INFO)";
    break;
  default:
    std::cout << " (SEVERITY : UNKNOWN)";
    break;
  }

  std::cout << std::endl;
}

GLFWwindow *glInit()
{
  glfwInit();

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow *window =
      glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "[IN55] ©Zoo 2019", NULL, NULL);

  if (window == NULL)
  {
    /* Could not create context */
    std::cout << "Could not create context for window" << std::endl;
    return NULL;
  }

  glfwMakeContextCurrent(window);

  glewExperimental = GL_TRUE; // Needed for core profile
  glewInit();

  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#ifndef __APPLE__ // Not supported in openGL under 4.4
  glEnable(GL_DEBUG_OUTPUT);
  glDebugMessageCallback(openglCallbackFunction, NULL);
#endif
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_DEPTH_TEST);

  std::cout << "OpenGL version : " << glGetString(GL_VERSION) << std::endl;

  return window;
}

void glDrawExp(GLFWwindow *window, Camera &activeCamera, std::vector<Scene *> scenes)
{
  glm::mat4 projection =
      glm::perspective(glm::radians(60.0f), 16.0f / 9.0f, 0.1f, 10000.0f);
  glm::mat4 view = activeCamera.getViewMatrix();
  glm::mat4 vp = projection * view;

  glUseProgram(activeCamera.getShaderProgram().getProgramId());

  GLint matrixId =
      glGetUniformLocation(activeCamera.getShaderProgram().getProgramId(),
                           SHADER_VIEW_PROJECTION_MATRIX_NAME);
  glUniformMatrix4fv(matrixId, 1, GL_FALSE, &vp[0][0]);

  glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  activeCamera.glUse(activeCamera.getShaderProgram());

  for (int i = 0; i < scenes.size(); i++)
  {
    scenes[i]->glDraw(activeCamera.getShaderProgram());
  }

  glfwSwapBuffers(window);
  glfwPollEvents();
}

LightConfig updateLightControle(GLFWwindow *window, ShaderProgram &shader, LightConfig *light)
{
  static LightConfig config{true, true, true};
  static bool updateUpKeyAmbiant = true, updateUpKeyDiffuse = true, updateUpKeySpecular = true;

  if (light != NULL)
  {
    config = *light;
  }

  if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS && (!updateUpKeyAmbiant))
  {
    config.enableAmbiant = !config.enableAmbiant;
    updateUpKeyAmbiant = true;
  }
  if (glfwGetKey(window, GLFW_KEY_F2) != GLFW_PRESS)
    updateUpKeyAmbiant = false;
  if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS && (!updateUpKeyDiffuse))
  {
    config.enableDiffuse = !config.enableDiffuse;
    updateUpKeyDiffuse = true;
  }
  if (glfwGetKey(window, GLFW_KEY_F3) != GLFW_PRESS)
    updateUpKeyDiffuse = false;
  if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_PRESS && (!updateUpKeySpecular))
  {
    config.enableSpecular = !config.enableSpecular;
    updateUpKeySpecular = true;
  }
  if (glfwGetKey(window, GLFW_KEY_F4) != GLFW_PRESS)
    updateUpKeySpecular = false;

  Light::glEnableLight(shader, config.enableAmbiant, config.enableDiffuse, config.enableSpecular);
  return config;
}

CameraRotateAround setupCamera(Scene &scene)
{
  BoundingBox bb = scene.calculateBoundingBox();
  std::cout << "size: " << glm::to_string(bb.min) << " x "
            << glm::to_string(bb.max) << std::endl;
  glm::vec3 target = 0.5f * (bb.min + bb.max);
  glm::vec3 eye = target + (2.0f * (bb.max - target));
  glm::vec3 direction = target - eye;
  glm::vec3 up(0, 1, 0);
  return CameraRotateAround(eye, target, up, WINDOW_WIDTH, WINDOW_HEIGHT);
}
