#ifndef FILES_H
#define FILES_H

#define GLFW_INCLUDE_GL3
#define GLFW_NO_GLU

#include <GL/glew.h>

#include <fstream>
#include <istream>
#include <iostream>
#include <streambuf>
#include <string>

namespace files{
  std::string* readFile(const std::string fileName);
}

#endif