#include "files.hpp"

std::string* files::readFile(const std::string file_name) {
  std::string *content = new std::string();
  std::ifstream file_stream("../resources/" + file_name, std::ifstream::in);

  if (!file_stream.is_open()) {
    std::cerr << "error reading file at ../resources/" << file_name
              << std::endl;
    std::exit(1);
  }

  content->assign((std::istreambuf_iterator<char>(file_stream)),
                  std::istreambuf_iterator<char>());

  file_stream.close();

  return content;
}
