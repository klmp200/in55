@REM @Author: Bartuccio Antoine
@REM @Date:   2019-06-14 11:03:30
@REM @Last Modified by:   klmp200
@REM Modified time: 2019-06-16 22:07:32

@echo off
set oldpath=%path%
set path=C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;%PROGRAMFILES%\CMake\bin\;C:\tools\git\cmd;C:\ProgramData\chocolatey\bin;C:\ProgramData\chocolatey\lib\mingw\tools\install\mingw64\bin

pushd build
cmake -G"MinGW Makefiles" ..
make
copy /Y external\glew\bin\Release\x64\glew32.dll glew32.dll
popd

set path=%oldpath%

echo End of compilation
pause