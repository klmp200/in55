#version 330 core

// model
out vec4 color;
in vec3 pnormal;
in vec3 pnormalPos;
in vec2 ptextCoords;

// camera

uniform mat4 model;
uniform mat4 vp;
uniform vec3 cameraPosition;

// materials property
uniform vec3 colorAmbient;
uniform vec3 colorDiffuse;
uniform vec3 colorEmissive;
uniform vec3 colorReflective;
uniform vec3 colorSpecular;
uniform float shininess;
uniform float shininessStrength;
uniform vec3 colorTransparent;

uniform sampler2D samplerTextureAmbient;
uniform sampler2D samplerTextureDiffuse;
uniform sampler2D samplerTextureDisplacement;
uniform sampler2D samplerTextureEmissive;
uniform sampler2D samplerTextureHeight;
uniform sampler2D samplerTextureLightmap;
uniform sampler2D samplerTextureNormals;
uniform sampler2D samplerTextureOpacity;
uniform sampler2D samplerTextureReflextion;
uniform sampler2D samplerTextureShininess;
uniform sampler2D samplerTextureSpecular;
uniform sampler2D samplerTextureUnknown;

uniform bool hasTextureAmbient;
uniform bool hasTextureDiffuse;
uniform bool hasTextureDisplacement;
uniform bool hasTextureEmissive;
uniform bool hasTextureHeight;
uniform bool hasTextureLightmap;
uniform bool hasTextureNormals;
uniform bool hasTextureOpacity;
uniform bool hasTextureReflextion;
uniform bool hasTextureShininess;
uniform bool hasTextureSpecular;
uniform bool hasTextureUnknown;

// light
struct Light {
  int type; // 0 is ambiente light, 1 is from a point, 2 is directional light
  vec3 position;
  vec3 direction;
  vec2 attenuation;
  vec3 color;
};

uniform int lightsCount;
uniform Light lights[3];

// enable light type
uniform bool enableAmbiantLight;
uniform bool enableDiffuseLight;
uniform bool enableSpecularLight;

// get texture
// blendType=0 -> addition, blendType=1 -> multiplication
vec3 getTexture(sampler2D sampler, bool hasSampler, vec3 baseValue,
                int blendType);

// light calculation
float calculateAttenuation(float distance, vec2 att);
vec3 calculateAmbientLight(int id);
vec3 calculateDiffuseLight(int id);
vec3 calculateSpeculareLight(int id);
vec3 getLightDirection(int id);

void main() {
  vec3 baseAmbiant =
      getTexture(samplerTextureAmbient, hasTextureAmbient, colorAmbient, 0);
  vec3 baseDiffuse =
      getTexture(samplerTextureDiffuse, hasTextureDiffuse, colorDiffuse, 0);
  vec3 baseSpecular =
      getTexture(samplerTextureSpecular, hasTextureSpecular, colorSpecular, 0);
  float opacity =
      getTexture(samplerTextureOpacity, hasTextureOpacity, vec3(1.0), 0).x;

  vec3 ambiant = vec3(0.0);
  vec3 diffuse = vec3(0.0);
  vec3 specular = vec3(0.0);

  for (int i = 0; i < lightsCount; ++i) {
    if (lights[i].type == 0 && enableAmbiantLight) { // add ambiante light
      ambiant += calculateAmbientLight(i);
    }
    if ((lights[i].type == 1 || lights[i].type == 2) &&
        enableDiffuseLight) { // add ambiante light
      diffuse += calculateDiffuseLight(i);
    }
    if ((lights[i].type == 1 || lights[i].type == 2) &&
        enableSpecularLight) { // add ambiante light
      specular += calculateSpeculareLight(i);
    }
  }
  // blend all light effect
  vec3 light =
      ambiant * baseDiffuse + diffuse * baseDiffuse + specular * baseSpecular;
  // add opacity
  color = vec4(light, opacity);
}

// get texture
vec3 getTexture(sampler2D sampler, bool hasSampler, vec3 baseValue,
                int blendType) {
  vec3 value = baseValue;
  if (hasSampler) {
    if (blendType == 0) {
      value += texture(sampler, ptextCoords).rgb; // blend ops
    } else if (blendType == 1) {
      value *= texture(sampler, ptextCoords).rgb; // blend ops
    }
  }
  return value;
}

float calculateAttenuation(float distance, vec2 att) {
  return 1.0 / (1.0 + att.x * distance + att.y * distance * distance);
}
vec3 getLightDirection(int id) {
  vec3 lightDir;
  if (lights[id].type == 1) { // if light come from a point
    lightDir = normalize(lights[id].position - pnormalPos);
  } else if (lights[id].type == 2) { // it's a projector
    lightDir = normalize(lights[id].direction);
  }
  return lightDir;
}

vec3 calculateDiffuseLight(int id) {
  vec3 norm = normalize(pnormal);
  vec3 lightDir = getLightDirection(id);
  float diff = max(dot(norm, lightDir), 0.0);
  return lights[id].color * diff;
}

vec3 calculateSpeculareLight(int id) {
  float specularStrength = shininessStrength;
  vec3 norm = normalize(pnormal);
  vec3 viewDir = normalize(cameraPosition - pnormalPos);
  vec3 lightDir = getLightDirection(id);
  vec3 reflectDir = reflect(-lightDir, norm);
  float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
  return specularStrength * spec * lights[id].color;
}

vec3 calculateAmbientLight(int id) { return lights[id].color; }