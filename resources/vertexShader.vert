#version 330 core

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 textCoords;
layout(location = 3) in vec4 weights;
layout(location = 4) in ivec4 boneIds;

out vec3 pnormal;
out vec3 pnormalPos;
out vec2 ptextCoords;

uniform mat4 model;
uniform mat4 vp;

const int MAX_BONES = 100;
const int MAX_BONES_PER_VERTEX = 4;
uniform mat4 boneTransforms[MAX_BONES];

void main() {
  if (weights[0] != 0){
    mat4 boneTransform = boneTransforms[boneIds[0]] * weights[0];
    for (int i = 1; i < MAX_BONES_PER_VERTEX; i++){
      if (weights[i] != 0){
        boneTransform += boneTransforms[boneIds[i]] * weights[i];
      }
    }
    gl_Position = vp * model * boneTransform * vec4(vertex, 1.0);
    pnormalPos = vec3(model * boneTransform * vec4(vertex, 1.0));
    pnormal = mat3(model * boneTransform) * normal;
  } else {
    gl_Position = vp * model * vec4(vertex, 1.0);
    pnormalPos = vec3(model * vec4(vertex, 1.0));
    pnormal = mat3(model) * normal;
  }

  //gl_Position = vp * model * vec4(vertex, 1.0);
  ptextCoords = textCoords;
}
